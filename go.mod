module gitlab.com/perinet/periMICA-container/service/flash_stm32g0x

go 1.21.3

require (
	gitlab.com/perinet/generic/apiservice/dnssd v0.0.0-20230628125338-c9bed005204b
	gitlab.com/perinet/generic/apiservice/staticfiles v0.0.0-20230511134106-f3597eb928e9
	gitlab.com/perinet/generic/lib/httpserver v0.0.0-20230721113448-51dceb287282
	gitlab.com/perinet/generic/lib/utils v0.0.0-20230628130910-2f4300736d64
	gitlab.com/perinet/periMICA-container/apiservice/flash_stm32g0x v0.0.0-20231106115548-6034a060af6b
	gitlab.com/perinet/periMICA-container/apiservice/lifecycle v0.0.0-20221101161231-d517bb9ab0d3
	gitlab.com/perinet/periMICA-container/apiservice/network v0.0.0-20221013145540-d41eb381fc53
	gitlab.com/perinet/periMICA-container/apiservice/node v0.0.0-20230404130054-c554bcd34ded
	gitlab.com/perinet/periMICA-container/apiservice/security v0.0.0-20230428094054-1af9a40cfc53
	gitlab.com/perinet/periMICA-container/apiservice/ssh v0.0.0-20221104093456-6bc24bfd2bae
)

require (
	github.com/alexandrevicenzi/go-sse v1.6.0 // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/godbus/dbus/v5 v5.1.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/grantae/certinfo v0.0.0-20170412194111-59d56a35515b // indirect
	github.com/holoplot/go-avahi v1.0.1 // indirect
	github.com/tidwall/gjson v1.14.3 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	gitlab.com/perinet/periMICA-container/app/flash_stm32g0x v0.0.0-20231106114900-1fd291c072b2 // indirect
	golang.org/x/exp v0.0.0-20230510235704-dd950f8aeaea // indirect
)
