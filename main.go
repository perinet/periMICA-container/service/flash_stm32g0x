/*
 * Copyright (c) Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

package main

import (
	"encoding/json"
	"log"

	"gitlab.com/perinet/generic/lib/httpserver"
	"gitlab.com/perinet/generic/lib/utils/webhelper"

	"gitlab.com/perinet/generic/apiservice/dnssd"
	"gitlab.com/perinet/generic/apiservice/staticfiles"

	"gitlab.com/perinet/periMICA-container/apiservice/lifecycle"
	"gitlab.com/perinet/periMICA-container/apiservice/network"
	"gitlab.com/perinet/periMICA-container/apiservice/node"
	"gitlab.com/perinet/periMICA-container/apiservice/security"
	"gitlab.com/perinet/periMICA-container/apiservice/ssh"
	"gitlab.com/perinet/periMICA-container/apiservice/flash_stm32g0x"
)

var (
	Logger         log.Logger = *log.Default()
	nodeInfo       node.NodeInfo
	productionInfo node.ProductionInfo
	securityConfig security.SecurityConfig
)

func init() {
	Logger.SetPrefix("Service periFLASH: ")

	Logger.Println("Starting")
	var data []byte
	// update apiservice node with used services
	services := []string{"node", "security", "lifecycle", "mqttclient", "dns-sd", "periflash", "ssh"}
	data, _ = json.Marshal(services)
	webhelper.InternalPut(node.ServicesSet, data)

	var err error
	data = webhelper.InternalGet(node.NodeInfoGet)
	err = json.Unmarshal(data, &nodeInfo)
	if err != nil {
		Logger.Println("Failed to fetch NodeInfo: ", err.Error())
	}

	err = json.Unmarshal(webhelper.InternalGet(node.ProductionInfoGet), &productionInfo)
	if err != nil {
		Logger.Println("Failed to fetch ProductionInfo: ", err.Error())
	}

	err = json.Unmarshal(webhelper.InternalGet(security.Security_Config_Get), &securityConfig)
	if err != nil {
		Logger.Println("Failed to fetch SecurityConfig: ", err.Error())
	}

	//start advertising _https via dnssd
	dnssdServiceInfo := dnssd.DNSSDServiceInfo{ServiceName: "_https._tcp", Port: 443}
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "api_version="+nodeInfo.ApiVersion)
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "life_state="+nodeInfo.LifeState)
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "application_name="+nodeInfo.Config.ApplicationName)
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "element_name="+nodeInfo.Config.ElementName)
	data, _ = json.Marshal(dnssdServiceInfo)
	webhelper.InternalVarsPatch(dnssd.DNSSDServiceInfoAdvertiseSet, map[string]string{"service_name": dnssdServiceInfo.ServiceName}, data)

	//start advertising _ssh via dnssd
	dnssdServiceInfo = dnssd.DNSSDServiceInfo{ServiceName: "_ssh._tcp", Port: 22}
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "api_version="+nodeInfo.ApiVersion)
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "life_state="+nodeInfo.LifeState)
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "application_name="+nodeInfo.Config.ApplicationName)
	dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "element_name="+nodeInfo.Config.ElementName)
	data, _ = json.Marshal(dnssdServiceInfo)
	webhelper.InternalVarsPut(dnssd.DNSSDServiceInfoAdvertiseSet, map[string]string{"service_name": dnssdServiceInfo.ServiceName}, data)
}

func restartServices() {
	httpserver.SetCertificates(httpserver.Certificates{
		CaCert:      webhelper.InternalVarsGet(security.Security_TrustAnchor_Get, map[string]string{"trust_anchor": "root_ca_cert"}),
		HostCert:    webhelper.InternalVarsGet(security.Security_Certificate_Get, map[string]string{"certificate": "host_cert"}),
		HostCertKey: webhelper.InternalVarsGet(security.Security_Certificate_Key_Get, map[string]string{"certificate": "host_cert"}),
	})

	httpserver.Restart()
}

func main() {
	httpserver.AddPaths(node.PathsGet())
	httpserver.AddPaths(security.PathsGet())
	httpserver.AddPaths(lifecycle.PathsGet())
	httpserver.AddPaths(network.PathsGet())
	httpserver.AddPaths(dnssd.PathsGet())
	httpserver.AddPaths(ssh.PathsGet())
	httpserver.AddPaths(staticfiles.PathsGet())

	httpserver.AddPaths(apiservice_flash_stm32g0x.PathsGet())

	security.SecurityEvent = restartServices

	httpserver.SetCertificates(httpserver.Certificates{
		CaCert:      webhelper.InternalVarsGet(security.Security_TrustAnchor_Get, map[string]string{"trust_anchor": "root_ca_cert"}),
		HostCert:    webhelper.InternalVarsGet(security.Security_Certificate_Get, map[string]string{"certificate": "host_cert"}),
		HostCertKey: webhelper.InternalVarsGet(security.Security_Certificate_Key_Get, map[string]string{"certificate": "host_cert"}),
	})

	httpserver.ListenAndServe("[::]:443")
}
